package routes

import (
	"github.com/gofiber/fiber/v2"

	"cvbuilder/controllers"
)

func DefineRoutes(app *fiber.App) {

	// Upload
	app.Get("/", func(ctx *fiber.Ctx) error {
		return controllers.UploadTestFile(ctx)
	})
	app.Post("/file", func(ctx *fiber.Ctx) error { 
		return controllers.UploadFile(ctx)
	})
	app.Post("/img", func(ctx *fiber.Ctx) error { 
		return controllers.UploadImg(ctx)
	})
	app.Post("/pdf", func(ctx *fiber.Ctx) error { 
		return controllers.UploadPdf(ctx)
	})
	// Home
	app.Get("/:name", func(ctx *fiber.Ctx) error {
		return controllers.Home(ctx)
	})

}