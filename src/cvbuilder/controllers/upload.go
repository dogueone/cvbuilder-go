package controllers

import (
	"fmt"
	"io/ioutil"

	"github.com/google/uuid"

	"cvbuilder/env"
	"cvbuilder/models"
	"cvbuilder/tools"

	"github.com/gofiber/fiber/v2"
)

func UploadFile(ctx *fiber.Ctx) error {
/*
	fileHeader, formErr := ctx.FormFile("myFile")

	if formErr != nil {
		return tools.JSONError(ctx, formErr)
	}

	if fileHeader.Size > env.MAX_FILE_SIZE {
		return tools.JSONMessage(ctx,
			fmt.Sprintf("File too big (max: %d MB)", env.MAX_FILE_SIZE / (1024 * 1024)),
			1,
		)
	}

	file, _ := fileHeader.Open()
	fileBytes, ioErr := ioutil.ReadAll(file)
*/
	body := ctx.Body()
	if int64(len(body)) > env.MAX_FILE_SIZE {
		return tools.JSONMessage(ctx,
			fmt.Sprintf("File too big (max: %d MB)", env.MAX_FILE_SIZE / (1024 * 1024)),
			1,
		)
	}

	fUuid := uuid.New().String()
	imgPath := tools.GetAbsPath("public", "img", fUuid + ".jpg")

	// defer os.Remove(imgPath)

	writeErr := ioutil.WriteFile(imgPath, body, 0644)

	if writeErr != nil {
		return tools.JSONError(ctx, writeErr)
	}

	// return that we have successfully uploaded our file!
	return tools.JSONMessage(ctx, "Successfully Uploaded File")
}

func UploadImg(ctx *fiber.Ctx) error {
	var img models.UploadImg
	parseErr := ctx.BodyParser(&img)
	if parseErr != nil {
		return tools.JSONError(ctx, parseErr)
		
	}

	if len(img.ImgUrl) == 0 {
		return tools.JSONMessage(ctx, "No base64 string was provided", 1)
	}

	result, msg := tools.WritePdfFromImg(img.ImgUrl)

	errorCode := 0
	if !result {
		errorCode = 1
	}

	return tools.JSONMessage(ctx, msg, uint8(errorCode))
}

func UploadPdf(ctx *fiber.Ctx) error {
	return tools.JSONMessage(ctx, "Not implemented yet", 1)
}

func UploadTestFile(ctx *fiber.Ctx) error {
	tools.WriteTestFile()
	return tools.JSONMessage(ctx, "OK")
}
