package controllers

import (
	"cvbuilder/tools"
	"fmt"
	"html"
	"net/url"

	"github.com/gofiber/fiber/v2"
)

func Home(ctx *fiber.Ctx) error {
	name, _ := url.QueryUnescape(html.UnescapeString(ctx.Params("name")))
	return tools.JSONMessage(ctx, fmt.Sprintf("Hello, %s", name))
}
