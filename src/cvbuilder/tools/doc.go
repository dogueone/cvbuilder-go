package tools

import (
	"bytes"
	"encoding/base64"
	"image"
	"math"
	"os"
	"regexp"
	"strings"

	"github.com/google/uuid"
	"github.com/jung-kurt/gofpdf"
)

func WritePdfFromImg(fileUrl string) (bool, string) {
	// Retrieve info from fileUrl
	re := regexp.MustCompile("^data:(.+)/(.+);base64,(.*)$")
	// whole | type | subtype | datastring
	matches := re.FindStringSubmatch(fileUrl)
	if matches == nil {
		return false, "RegExp found no matches"
	}
	// First match is not needed
	_, kind, ext, b64 := matches[0], matches[1], matches[2], matches[3]

	// If kind is not "image" we have no use of the data
	if kind != "image" {
		return false, "b64 string is not of an image"
	}

	// Convert b64 string to byte array
	imgBytes, _ := base64.StdEncoding.DecodeString(b64)
	// Create uuid we will use for both image and resulting pdf file
	fUuid := uuid.New().String()

	// Get absolute path of public/img since ioutils work at system level
	// Set image save path
	imgPath := GetAbsPath("public", "img", fUuid + "." + ext)
	// Write bytes into a file with standard permissions
	imgErr := os.WriteFile(imgPath, imgBytes, 0644)

	if imgErr != nil {
		return false, imgErr.Error()
	}

	// This is async and will take place at the end of the function
	defer os.Remove(imgPath)

	// Everything good, we can go on creating the PDF Object
	// Using fpdf for its popularity on several programming languages
	pdf := gofpdf.New("P", "mm", "A4", "")
	pdf.AddPage()
	pdf.AddUTF8Font("Verdana", "B", GetAbsPath("assets", "fonts", "verdana", "VerdanaBold.ttf"))
	pdf.SetFont("Verdana", "B", 16)
	
	// Get Background Image
	bkgPath := GetAbsPath("assets", "img", "temp.png")
	bkgFile, bkgErr := os.ReadFile(bkgPath)

	// File exists and has been loaded
	if bkgErr == nil {
		bImg, _, bDecodeErr := image.Decode(bytes.NewReader(bkgFile))
		// We proceed
		if bDecodeErr == nil && bImg != nil {
			bImgOpts := gofpdf.ImageOptions{ImageType: "PNG", ReadDpi: true, AllowNegativePosition: false}
			pdf.ImageOptions(bkgPath, 0, 0, 210, 297, false, bImgOpts, 0, "")
		}
	}
	// Get Sent Image metadata
	reader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(b64))
	// image | format | error
	m, _, decodeErr := image.Decode(reader)

	if decodeErr != nil {
		return false, decodeErr.Error()
	}

	if m == nil {
		return false, "Decoded image is null"
	}

	readDpi := true
	if ext != "png" { readDpi = false }
	imgopts := gofpdf.ImageOptions{ImageType: strings.ToUpper(ext), ReadDpi: readDpi, AllowNegativePosition: false}

	pdf.ImageOptions(imgPath, 16, 29, 34, 34, false, imgopts, 0, "")
	// Get absolute path of public/img since ioutils work at system level
	// Set PDF save path
	pdfPath := GetAbsPath("public", "pdf", fUuid + ".pdf")
	pdfErr := pdf.OutputFileAndClose(pdfPath)

	if pdfErr != nil {
		return false, pdfErr.Error()
	}

	return true, "OK"
}



func WriteTestFile() bool {
	// Font sizes
	var titleSize float64 = 30
	var subTitleSize float64 = 20
	var bodySize float64 = 10
	var bodyLineHeight float64 = math.Round(bodySize * 0.75)
	tUuid := uuid.New().String()

	// footerSVG := getAsset("img", "footer.png")
	// verdanaBytes := getAsset("fonts", "verdana/verdana.ttf")
	// verdanaBoldBytes := getAsset("fonts", "verdana/verdanaBold.ttf")

	pdf := gofpdf.New("P", "mm", "A4", "")
	pdf.AddPage()
	// footerImg, _, _ := image.Decode(bytes.NewReader(footerSVG))
	// footerDims := footerImg.Bounds().Size()



	// Fonts
	pdf.AddUTF8Font("Verdana", "", GetAbsPath("assets", "fonts", "verdana", "verdana.ttf"))
	pdf.AddUTF8Font("Verdana", "B", GetAbsPath("assets", "fonts", "verdana", "VerdanaBold.ttf"))
	
	// General Image Options
	pngOpts := gofpdf.ImageOptions{ImageType: "PNG", ReadDpi: true, AllowNegativePosition: true}
	// Add Footer Image
	pdf.ImageOptions(GetAbsPath("assets", "img", "footer.png"), 0, 297 - 35, 210, 35, false, pngOpts, 0, "")
	// Add Logo Image
	pdf.ImageOptions(GetAbsPath("assets", "img", "CapLogo.png"), 10, 8, 82, 20, false, pngOpts, 0, "")
	// Add Avatar Circle
	pdf.SetDrawColor(0, 135, 181)
	pdf.Circle(29, 50, 17, "F")
	// Set Text Color
	pdf.SetTextColor(0, 135, 181)
	// Add Personal Data
	pdf.SetFont("Verdana", "B", titleSize)
	pdf.Text(58, 50, "Filip Nilsson")
	// Add Role
	pdf.SetFont("Verdana", "B", subTitleSize)
	pdf.Text(58, 65, "Managing Software Engineer")
	// Add Description
	descRows := LineBreaks("Beef ribs buffalo ball tip pork loin picanha shoulder kielbasa strip steak short loin.  Tail pastrami pork loin cow salami pancetta andouille boudin ham pork doner strip steak kevin.  Andouille pork chop filet mignon burgdoggen beef ribs prosciutto chuck beef bacon tri-tip sausage shank.  Pork belly bacon tri-tip frankfurter beef ribs tenderloin fatback leberkas sausage ham rump.  Prosciutto ham hock bacon ground round shoulder strip steak swine tongue shank bresaola turkey ball tip leberkas.  Spare ribs salami prosciutto sausage chislic biltong corned beef buffalo filet mignon ham swine flank turkey.  Sausage kielbasa filet mignon shank leberkas tri-tip.", 66)
	pdf.SetFont("Verdana", "", bodySize)

	for idx, row := range descRows {
		pdf.Text(58, 75 + float64(idx) * bodyLineHeight, row)
	}

	// Set PDF save path
	pdfPath := GetAbsPath("public", "pdf", tUuid + ".pdf")
	pdfErr := pdf.OutputFileAndClose(pdfPath)

	if pdfErr != nil {
		return false
	}

	return true
}