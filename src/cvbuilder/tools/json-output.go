package tools

import (
	"cvbuilder/models"
	"encoding/json"

	"github.com/gofiber/fiber/v2"
)

func jsonOutput(ctx *fiber.Ctx, obj interface{}) error {
	js, _ := json.Marshal(obj)
	ctx.Response().Header.Set("Content-Type", "application/json")
	return ctx.Send(js)
}

func JSONMessage(ctx *fiber.Ctx, message string, code ...uint8) error {
	var msg models.JMessage
	if len(code) > 0 {
		msg = models.JMessage{Error: code[0], Message: message}
	} else {
		msg = models.JMessage{Error: 0, Message: message}
	}
	
	return jsonOutput(ctx, msg)
}

func JSONError(ctx *fiber.Ctx, err error) error {
	msg := models.JMessage{Error: 1, Message: err.Error()}
	return jsonOutput(ctx, msg)
}