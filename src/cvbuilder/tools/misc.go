package tools

import (
	"os"
	"path/filepath"
	"strings"
)

func GetAbsPath(subpaths ...string) string {
	pwd, _ := os.Getwd()
	joined := filepath.Join(subpaths...)
	return filepath.Join(pwd, joined)
}

func getAsset(kind string, name string) []byte {
	path := GetAbsPath("assets", kind, name)
	file, err := os.ReadFile(path)

	if err != nil {
		return nil
	}

	return file
}

func LineBreaks(text string, max int) []string {
	words := strings.Split(text, " ")
	result := []string{}
	tmp := []string{}

	for _, word := range words {
		sTmp := strings.Join(tmp, " ")
		// Line break in case of current line length would overflow
		// or if a double space is encountered, as it would be a
		// non-rendered line-break character
		if len(sTmp) + len(word) > max || len(word) == 0 {
			result = append(result, sTmp)
			tmp = make([]string, 0)
		}

		if len(word) > 0 {
			tmp = append(tmp, word)
		}
	}

	return result
}