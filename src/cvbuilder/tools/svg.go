package tools

import (
	"bytes"
	"image"
	"image/png"
	"os"

	"github.com/google/uuid"
	"github.com/jung-kurt/gofpdf"
	"github.com/srwiley/oksvg"
	"github.com/srwiley/rasterx"
)

func CreatePDF(svgstr []string) (string, error) {
	pdf := gofpdf.New("P", "mm", "A4", "")
	pdfPath := GetAbsPath("public", "pdf", uuid.New().String() + ".pdf")

	imgOpts := gofpdf.ImageOptions{ImageType: "PNG", ReadDpi: true, AllowNegativePosition: false}
	imgPaths := []string{}

	for _, str := range svgstr {
		pdf.AddPage()
		imgPath := svg2png(str, 210, 297)
		imgPaths = append(imgPaths, imgPath)

		if len(imgPath) == 0 {
			continue
		}

		pdf.ImageOptions(imgPath, 0, 0, 210, 297, false, imgOpts, 0, "")
	}

	pdfErr := pdf.OutputFileAndClose(pdfPath)

	removeTempImages(imgPaths)

	if pdfErr != nil {
		return "", pdfErr
	}

	return pdfPath, nil
}

func svg2png(str string, w int, h int) string {
	reader := bytes.NewReader([]byte(str))

	tmpPng := GetAbsPath("public", "img", uuid.New().String() + ".png")

  img, _ := oksvg.ReadIconStream(reader)
  img.SetTarget(0, 0, float64(w), float64(h))
  rgba := image.NewRGBA(image.Rect(0, 0, w, h))
  img.Draw(rasterx.NewDasher(w, h, rasterx.NewScannerGV(w, h, rgba, rgba.Bounds())), 1)

  out, err := os.Create(tmpPng)
  if err != nil {
    panic(err)
  }
  defer out.Close()

  err = png.Encode(out, rgba)
  if err != nil {
    panic(err)
  }

	return tmpPng
}

func removeTempImages(paths []string) {
	for _, path := range paths {
		os.Remove(path)
	}
}