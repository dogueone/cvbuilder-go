package models;

type JMessage struct {
	Error uint8
	Message string
}