package main

import (
	"cvbuilder/routes"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()
	routes.DefineRoutes(app)

	app.Listen(":8080")
}