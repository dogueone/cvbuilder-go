package env

const MAX_FILE_SIZE int64 = 2 << 20 // 2 MB
const SERVER_PORT = "8080"
